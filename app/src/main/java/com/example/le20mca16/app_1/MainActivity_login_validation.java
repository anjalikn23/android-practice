package com.example.le20mca16.app_1;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity_login_validation extends Activity {


    private EditText et_Username;
    private EditText et_Password;
    private Button bt_SignIn;
    private TextView tv_Message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Intent intent = new Intent(this,Student_details.class);
        startActivity(intent);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_login_validation);

        et_Username = (EditText) findViewById(R.id.et_Username);
        et_Password = (EditText) findViewById(R.id.et_Password);
        bt_SignIn = (Button) findViewById(R.id.bt_SignIn);
        tv_Message = (TextView) findViewById(R.id.tv_Message);

        bt_SignIn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {

                String username = String.valueOf(et_Username.getText());
                String password = String.valueOf(et_Password.getText());
                if (username.equals("admin") && password.equals("admin")) {
                    tv_Message.setText("Login Successful!");
                } else {
                    tv_Message.setText("Login Unsuccessful!");
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.login, menu);
        return true;
    }

}

