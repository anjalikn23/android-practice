package com.example.le20mca16.login_page;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import static android.R.string.ok;

public class Login extends Activity implements OnClickListener
{
    Button login,clear;
    AlertDialog dialog;
    EditText unm,pwd;
    @Override
    public void onCreate(Bundle savedinstance)
    {
        super.onCreate(savedinstance);
        setContentView(R.layout.activity_main_loign);
        login=(Button)findViewById(R.id.login);
        dialog=new AlertDialog.Builder(this).create();
        dialog.setTitle("MSG“);
        this.login.setOnClickListener(this);
        clear=(Button)findViewById(R.id.clear);
        this.clear.setOnClickListener(this);

    }
    public void onClick(View v)
    {
        if(v==this.login)
        {
            unm=(EditText)findViewById(R.id.unm);
            pwd=(EditText)findViewById(R.id.pwd);
            String un=unm.getText().toString();
            String pw=pwd.getText().toString();
            if(un.equals(“”)||pw.equals(“”))
            {
                dialog.setMessage(” Enter Name & Password  “);
                dialog.setButton(” ok”, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1)
                    {
                        return;
                    }
                });
            }
            else
            {
                if(unm.equals(“anjali”)||pwd.equals(“anjali”))
                {
                    //dialog.setMessage(” Yahoooo  “);
                    Intent menuintent=new Intent(Demo.this,MenuControl.class);
                    startActivity(menuintent);

                }
                else
                {
                    dialog.setMessage(” UserName or Password Error  “);
                }
                dialog.setButton(” ok”, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        return;

                    }
                });
            }
            dialog.show();

        }
        else
        {
            unm.setText(“”);
            pwd.setText(“”);
        }

    }
}